import unittest
import requests
from http import HTTPStatus

class MyTestCase(unittest.TestCase):

    base_path = 'http://localhost:5000'

    def test_smoke_test(self):
        """ Just checking that the server has been launched... """
        try:
            r = requests.get(MyTestCase.base_path)
        except requests.ConnectionError:
                self.assertEqual(True, False, 'Did you launch the Flask server ?')
        assert True

    def test_hello_world(self):
        hello_world_path = self.base_path
        r = requests.get(hello_world_path)
        self.assertEqual(r.status_code, HTTPStatus.OK)
        self.assertIn("Hello World", str(r.content), f"Hello World has  "
                                                         f"not been found on {hello_world_path}")

    def test_titanic_index(self):
        url = self.base_path + '/titanic'
        r = requests.get(url)
        self.assertEqual(r.status_code, HTTPStatus.OK)
        self.assertIn("Titanic", str(r.content), f"Titanic has  "
                                                     f"not been found on "
                                                 f"{url}")

    def test_titanic_add(self):
        ''' TODO : Adding a record should work. '''
        pass
    def test_titanic_add_erronoeus_form(self):
        ''' TODO : Adding a record whilst omiting required fields '''
        pass
    def test_titanic_edit_correct_form(self):
        ''' TODO : Editing a record should be successful. '''
        pass
    def test_titanic_edit_erroneous_form(self):
        ''' TODO : Error in filled out edit form. '''
        pass
    def test_titanic_edit_invalid_id(self):
        """ TODO : Editing an id that does not exist"""
        pass

if __name__ == '__main__':
    unittest.main()
